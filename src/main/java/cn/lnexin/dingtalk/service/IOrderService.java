package cn.lnexin.dingtalk.service;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * <p>
 * 钉钉应用下单 服务类
 * </p>
 *
 * @author lnexin@aliyun.com
 * @since 2019-09-11
 */
public interface IOrderService {
    /**
     * 下单之后初始化到数据库
     * @param payNode
     * @return
     */
    Boolean callbackInit(JsonNode payNode);
}
