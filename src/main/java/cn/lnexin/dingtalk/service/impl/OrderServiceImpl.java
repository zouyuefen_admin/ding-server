package cn.lnexin.dingtalk.service.impl;

import cn.lnexin.dingtalk.entity.Order;
import com.fasterxml.jackson.databind.JsonNode;
import cn.lnexin.dingtalk.service.IOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钉钉应用下单 服务实现类
 * </p>
 *
 * @author lnexin@aliyun.com
 * @since 2019-09-11
 */
@Service
public class OrderServiceImpl implements IOrderService {

    /**
     * {
     * "EventType": "market_buy",
     * "SuiteKey": "suited6db0pze8yao1b1y",
     * "buyCorpId": "dingxxxxxxxx",
     * "goodsCode": "FW_GOODS-xxxxxxxx",
     * "itemCode": "1c5f70cf04c437fb9aa1b20xxxxxxxx",
     * "itemName": "按照范围收费规格0-300",
     * "subQuantity": 1（订购的具体人数）,
     * "maxOfPeople": 300,
     * "minOfPeople": 0,
     * "orderId": 308356401xxxxxxxx,
     * "paidtime": 1474535702000,
     * "serviceStopTime": 1477065600000,
     * "payFee":147600,
     * "orderCreateSource":"DRP",
     * "nominalPayFee":147600,
     * "discountFee":600,
     * "discount":0.06,
     * "distributorCorpId":"ding9f50b15bccd16741",
     * "distributorCorpName":"测试企业"
     * }
     */
    @Override
    public Boolean callbackInit(JsonNode payNode) {
        Order order = new Order();
        order.setOrderId(payNode.get("orderId").longValue());
        order.setSuiteKey(payNode.get("SuiteKey").textValue());
        order.setCorpId(payNode.get("buyCorpId").textValue());
        order.setGoodsCode(payNode.get("goodsCode").textValue());

        order.setItemCode(payNode.get("itemCode").textValue());
        order.setItemName(payNode.get("itemName").textValue());
        order.setMaxPeople(payNode.get("maxOfPeople").intValue());
        order.setMinPeople(payNode.get("minOfPeople").intValue());
        order.setPaidtime(payNode.get("paidtime").longValue());
        order.setServiceStopTime(payNode.get("serviceStopTime").longValue());

        order.setPayFee(payNode.get("payFee").longValue());
        order.setOrderCreateSource(payNode.get("orderCreateSource").textValue());
        order.setNominalPayFee(payNode.get("nominalPayFee").longValue());
        order.setDiscountFee(payNode.get("discountFee").longValue());
        order.setDiscount(payNode.get("discount").decimalValue());
        order.setDistributorCorpId(payNode.get("distributorCorpId").textValue());
        order.setDistributorCorpName(payNode.get("distributorCorpName").textValue());


        return true;
    }
}
