package cn.lnexin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 钉钉示例项目启动入口
 * <p>
 *
 * @author lnexin@aliyun.com
 */
@SpringBootApplication
@EnableAutoConfiguration
public class DingApplication {
    public static void main(String[] args) {
        SpringApplication.run(DingApplication.class);
    }

}
